#include <ctime> 
#include "date.h"
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
// #include <sstream>      // std::stringstream

using std::string;
// using std::cin;
using std::endl;
using std::ofstream;
using std::ostream;
// using std::istream;
// using std::ostream;
// using std::ofstream;
// using std::ifstream;



template <typename T>
std::string toString(T a)
{
    std::stringstream stream;
    stream << a;
    return stream.str();
}

int run(ostream& fout){
	try {
		double d = 1.234;
		Date aday(2019, 2, 14);
		std::string sd = toString(d);
		fout << sd <<  endl;
		std::string st = toString(aday);
		fout << st <<  endl;
		
	} catch (std::invalid_argument& e) {
		
		fout << "Error: " << e.what() << endl;
		
	}
	return 0;	
}



int main() {
	ofstream fout( "./tests/a4_output.txt" );
	run(fout);
	return 0;
}











