
#include <iostream>
#include <fstream>
#include <string>
#include "tagRemover.h"

using std::string;
using std::cin;
using std::cout;
using std::endl;
using std::istream;
using std::ostream;
using std::ofstream;
using std::ifstream;


void run_TagRemover(istream& i, ostream& o){
	TagRemover tr(i);
	tr.print(o);
}

int main(int argc, const char** argv) {
	if(argc > 1){
		const string inname = argv[1];
		ifstream infile( inname );
		if(argc > 2){
			const string outname = argv[2];
			ofstream fout( outname );
			run_TagRemover(infile, fout);
		} else {
			run_TagRemover(infile, std::cout);
		}
	} else  {
		run_TagRemover(std::cin, std::cout);
	}
	return 0;
}
