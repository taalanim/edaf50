#include <string>
#include <vector>
#include <fstream>
#include <iostream>
#include <algorithm>
#include "tagRemover.h"
using std::istream;
using std::ostream;
using std::cout;
using std::endl;


TagRemover::TagRemover(istream& stream) : in_stream(stream) {
	
}

void print_the_thing(ostream& out_stream, char c, bool in_a_tag, bool in_a_spec_char){
	if (c == '\n'){
		out_stream << c;
	} else {
		if (!in_a_tag && !in_a_spec_char){
			out_stream << c;
		}
	}
}

void delay(bool d_tag, bool& tag, bool d_car, bool& car){
	tag = d_tag;
	car = d_car;
	d_tag = false;
	d_car = false;
}

void TagRemover::print(ostream& out_stream) const{
	// out_stream << "hello" << endl;
	char c;
	bool tag = false;
	bool d_tag = false;
	bool car = false;
	bool d_car = false;
	while (in_stream.get(c)){
		delay( d_tag, tag, d_car, car);
		if (tag){
			if(c == '>'){
				d_tag = false;
			}
		} else {
			if(c == '<'){
				tag = true;
				d_tag = true;
			} else if (c == '&'){
				car = true;
				d_car = true;
			} else if (car == true && (c == 't' || c == 'p' )) {
				d_car = false;
			}
		}
		print_the_thing(out_stream, c, tag, car);
	}
	// out_stream << endl;
	
}

