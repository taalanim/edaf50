
#include <iostream>
#include <fstream>
#include <string>
#include "tagRemover.h"

using std::string;
using std::cin;
using std::cout;
using std::endl;
using std::istream;
using std::ostream;
using std::ofstream;
using std::ifstream;


void run(string& workspace){
	workspace.at(0) = 'c';
	workspace.at(1) = 'c';
	for (size_t i = 0; i < workspace.length(); i++){
		if(workspace.at(i) == 'p'){
			for (size_t j = 2 * i ; j < workspace.length(); j += i){
				workspace.at(j) = 'c';
			}	
		}	
	}	
}


void print(ostream& fout, string& workspace){
	const size_t print_size =  200;
	for (size_t i = 0; i < print_size; i++){
		if (workspace.at(i) == 'p'){
			if (i < 10 ) { fout << " ";}
			if (i < 100 ) { fout << " ";}
			fout << i << "\t";
		}
		if (i%10 == 0 && i != 0) { fout << "\n";}
	}
	
	fout << "\n\n" << workspace.find_last_of('p') << endl;
	

}

int main() {
	const size_t max_size = 100000;
	ofstream fout( "./tests/prime_output.txt" );
	string workspace (max_size, 'p');	
	// cout << "d" << endl;
	run(workspace);
	print(fout, workspace);
	
	return 0;
}


