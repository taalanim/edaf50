#include <iostream>
#include <string>
#include <algorithm>
#include <vector>
#include <cctype>
#include "dictionary.h"

using std::string;
using std::vector;
using std::cin;
using std::cout;
using std::endl;

void check_word(const string& word, const Dictionary& dict)
{
    if (word == "yolo" || word == "YOLO"){
        cout << "Give up on life NOW" << endl;
        return;
    }

    if (dict.contains(word)) {
        cout << "Correct." << endl;
    } else {
        vector<string> suggestions = dict.get_suggestions(word);
        if (suggestions.empty()) {
            cout << "Wrong, no suggestions." << endl;
        } else {
            cout << "Wrong. Suggestions:" << endl;
            for (const auto& w : suggestions) {
                cout << "    " << w << endl;
            }
        }
    }
}
int main(int argc, const char** argv) {
    Dictionary dict;
    string word;
    if(argc > 1){

        for (int i = 1;i < argc; i++ ){
            word = argv[i];

            cout  << word;
            for (unsigned int i = 0; i < 25-word.length(); i++ ){
                cout << "-";
            }
            cout <<"[" << i-1 << "]" << endl;
            transform(word.begin(), word.end(), word.begin(), ::tolower);
            check_word(word, dict);
        }
    } else  {
        while (cin >> word) {
            transform(word.begin(), word.end(), word.begin(), ::tolower);
            check_word(word, dict);
        }
    }
    return 0;
}
