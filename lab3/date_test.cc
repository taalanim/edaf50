#include <iostream>
#include <iomanip> // for setw and setfill
#include "date.h"

using std::cin;
using std::cout;
using std::endl;
using std::setw;
using std::setfill;

/*
* Prints the date d in the format yyyy-mm-dd. You shall replace this
* function with an overloaded operator<<, and add an overloaded operator>>.
*
*/
void print(const Date& d) {
	cout << setw(4) << setfill('0') << d.getYear() << '-';
	cout << setw(2) << setfill('0') << d.getMonth() << '-';
	cout << setw(2) << setfill('0') << d.getDay();
}


void do_test(){
	
	// Check 'next' by creating an object describing today's date, then
	// printing dates more than a month ahead
	cout << "--- Today and more than a month ahead:" << endl;
	Date d1;
	print(d1);
	cout << endl;
	for (int i = 1; i <= 35 ; ++i) {
		d1.next();
		print(d1);
		cout << endl;
	}
	
	// Check so 'next' functions correctly from one year to the next
	cout << "--- New Year's Eve and the next day:" << endl;
	Date d2(2013, 12, 31);
	print(d2);
	cout << endl;
	d2.next();
	print(d2);
	cout << endl;
}

void do_interactive(){
	bool cont = true;
	while (cont) {
		cout << "Type a date: ";
		Date aDate;
		cin >> aDate;
		if (cin.eof()) {
			cont = false;
		} else if (!cin.good()) {
			cout << "Wrong input format" << endl;
			// restore stream state and ignore the rest of the line
			cin.clear();
			cin.ignore(10000, '\n');
		}
		else {
			cout << "Output: " << aDate << endl;
		}
	}
}

int main(int argc, const char** argv) {
	bool test = false;
	for (int i = 1; i < argc; ++i) {
		if (std::string(argv[i]) == "test") {
			test = true;
		}
	}
	
	if(test){
		do_test();
	} else {
		do_interactive();
	}
	
	
}






