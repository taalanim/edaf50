#ifndef DICTIONARY_H
#define DICTIONARY_H

#include <string>
#include <vector>

class Dictionary {
public:
	Dictionary();
	bool contains(const std::string& word) const;
	void print_vectors() const;
	std::vector<std::string> get_suggestions(const std::string& word) const;
private:
	void gather_suggestions(const std::string& word , std::vector<std::string> &suggestions) const;
	void rank_suggestions(const std::string& word , std::vector<std::string> &suggestions) const;
	unsigned int get_ed_dist(const std::string& word1, const std::string& word2) const;
	void trim_suggestions( std::vector<std::string> &suggestions) const;

};

#endif
