#include <ctime>  // time and localtime
#include "date.h"

#include <iostream>
#include <fstream>
#include <string>
#include "tagRemover.h"

using std::string;
using std::cin;
using std::cout;
using std::endl;
using std::istream;
using std::ostream;
using std::ofstream;
using std::ifstream;

int Date::daysPerMonth[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

Date::Date() {
	time_t timer = time(0); // time in seconds since 1970-01-01
	tm* locTime = localtime(&timer); // broken-down time
	year = 1900 + locTime->tm_year;
	month = 1 + locTime->tm_mon;
	day = locTime->tm_mday;
}
Date::Date( string r) {
	string s = r;
	std::string delimiter = "-";
	size_t pos = 0;
	std::string token1;
	std::string token2;
	std::string token3;
	
	    token1 = s.substr(0, pos);
	    s.erase(0, pos + delimiter.length());
	
	    token2 = s.substr(0, pos);
	    s.erase(0, pos + delimiter.length());
	
	    token3 = s.substr(0, pos);
		s.erase(0, pos + delimiter.length());
	
	
	
	
	
	int a = std::stod(token1);
	int b = std::stod(token2);
	int c = std::stod(token3);
	Date(a,b,c);


}

// Date::Date(int y, int m, int d) {}

int Date::getYear() const {
	return year;
}

int Date::getMonth() const {
	return month;
}

int Date::getDay() const {
	return day;
}

void Date::next() {
	day ++;
	if (day > daysPerMonth[month-1]){
		month++;
		day = 0;
		if (month > 12){
			year ++;
			month = 0;
			if (year > 9999){
				year = 0;
			}
		}
	} 
}







