#include <iostream>
#include "coding.h"
#include <string>
// #include <cassert>
#include <fstream>
#include <sstream>
using std::cout;
using std::endl;
using std::string;
using std::ifstream;
using std::ofstream;
// using namespace std;

string choose_outname(int argc, const char** argv){
    if( argc == 3 ){
        return argv[2];
    } else {
        return "default_encode_output.txt";
    }
}

string choose_inname(int argc, const char** argv){
    if( argc >= 2 ){
        return argv[1];
    } else {
        return "default_encode_input.txt";
    }
}


int main(int argc, const char** argv){

    string inname = choose_inname( argc, argv );
    ifstream infile( inname );
    string outname = choose_outname( argc, argv );
    ofstream fout( outname );

    if( fout ){

        string line;
        getline( infile, line );
        for(unsigned int i = 0; i < line.length(); i++){
            fout << encode(line.at(i));
        }
        for(; getline( infile, line ); ){
            fout << "\n";
            for(unsigned int i = 0; i < line.length(); i++){
                fout << encode(line.at(i));
            }
        }

        fout << endl;

    } else {
        std::cerr << "Failure opening " << argv[1] << '\n';
        return -1;
    }

    // cout << "input file     =  " << inname << "\nencoded output =  " << outname << " \n\n" <<  endl;
    return 0;
}
