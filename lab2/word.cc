#include <string>
#include <vector>
#include <iostream>
#include "word.h"

using std::vector;
using std::string;
using std::cout;
using std::endl;

// Word::Word(const string& w, const vector<string>& t) {
// 	word = w;
// 	tripps = t;
// }


void Word::print() const {
	// cout << "-------" << endl;;
	cout << word << "---";
	for (auto i = tripps.begin(); i != tripps.end(); ++i)
	cout << *i << "-";
	cout << endl;
}

unsigned int Word::get_matches(const vector<string>& t) const {
	unsigned int k = 0;


	vector<string>::const_iterator a = tripps.begin();
	vector<string>::const_iterator b = t.begin();

	while(a != tripps.end() && b != t.end()){

		string aa = *a;
		string bb = *b;

		if(aa.compare(bb) == 0){
			a++;
			b++;
			k++;
		} else if (aa.compare(bb) < 0){
			a++;
		} else{
			b++;
		}
	}
	return k;
}
