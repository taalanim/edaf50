#include <string>
#include <vector>
#include <fstream>
#include <iostream>
#include <algorithm>
#include <unordered_set>
#include <map>
#include "word.h"
#include "dictionary.h"
using std::string;
using std::vector;
using std::cout;
using std::endl;
using std::unordered_set;
using std::ifstream;
using std::multimap;
// using namespace std;


unordered_set<string> list;
const int wordlen = 25;
vector<Word> words[wordlen]; 	// words[i] = the words with i letters,
// ignore words longer than 25 letters

vector<std::string> tokenize(string const &in) {
	char sep = ' ';

	string::size_type pos;

	unsigned int items = std::stoi(in, &pos);

	vector<std::string> result;
	if(items != 0){

		while ((pos = in.find_first_not_of(sep, pos)) != std::string::npos) {
			auto e = in.find_first_of(sep, pos);
			result.push_back(in.substr(pos, e-pos));
			pos = e;
		}
	}
	return result;
}



void load_word(string* a){
	string line = *a;
	if (line.length() > 1){ // since it ends with empty line

		unsigned int b = line.find_first_of(" ");

		string bit1 (line, 0, b);
		string bit2 = line.substr(b);

		vector<std::string> v = tokenize(bit2);

		Word the_word(bit1, v);

		words[b].insert(words[b].end(), the_word);

		list.insert(bit1);
	}
}


void Dictionary::print_vectors() const{
	for(int i = 0; i < wordlen; i++){
		cout << "\n\n" << i << "---"<< words[i].size() << "\n\n";
		// Word w;
		for(vector<Word>::iterator it = words[i].begin(); it != words[i].end(); ++it) {
			Word w = *it;
			w.print();
		}
	}
	cout << endl;
}



Dictionary::Dictionary() {
	ifstream infile( "words.txt" );
	for(string line; getline( infile, line ); ){
		load_word(&line);
	}
	// print_vectors();
}



bool Dictionary::contains(const string& word) const {
	const auto search = list.find(word);
	return search != list.end();
}



vector<std::string> get_other_things(string *a){
	string line = *a;
	vector<string> bits;
	if(line.length() > 2){
		for(unsigned int i = 0; i < line.length() - 2 ; i++){
			string bit (line, i, 3);

			for_each(bit.begin(), bit.end(), [](char & c){
				c = ::tolower(c);
			});
			bits.push_back(bit);
		}
	}
	sort(bits.begin(),bits.end());
	return bits;
}


void Dictionary::gather_suggestions(const string& word, vector<string> &suggestions) const{
	bool smart = false;

	for( int i = -1; i < 2; i ++){
		if(word.length()+ i < wordlen){
			// cout << "a" << word.length()+ i << endl;
			for(vector<Word>::iterator it = words[word.length()+ i].begin(); it != words[word.length()+ i].end(); ++it) {
				Word w = *it;
				// w.print();
				string temp = word;
				if(smart){

					unsigned int u = w.get_matches(get_other_things(&temp));
					if(u > word.length()/2 || (word.length() < 10 && u >= 2)|| (word.length() < 5)){// FIXME prob, I mean it works, bt might be hard to see results of later code
						suggestions.push_back(w.get_word());
						// 	// cout << suggestions.size()<< endl;
					}
				} else {
					suggestions.push_back(w.get_word());
				}
			}
		}
	}
}


unsigned int Dictionary::get_ed_dist(const string& word1, const string& word2) const{
	const unsigned int len1 = word2.size();
	const unsigned int len2 = word1.size();
	vector<vector<unsigned int>> d(len1 + 1, vector<unsigned int>(len2 + 1));

	d[0][0] = 0;
	for(unsigned int i = 1; i <= len1; ++i){
		d[i][0] = i;
	}

	for(unsigned int i = 1; i <= len2; ++i){
		d[0][i] = i;
	}

	unsigned int i;
	unsigned int j;
	unsigned int a;
	unsigned int b;
	unsigned int c;

	for(i = 1; i <= len1; ++i){
		for(j = 1; j <= len2; ++j){
			a = d[i - 1][j] + 1;
			b = d[i][j - 1] + 1;
			c =  d[i - 1][j - 1] + (word2[i - 1] == word1[j - 1] ? 0 : 1);
			d[i][j] = std::min( std::min(a , b) , c );
		}
	}

	return d[len1][len2];
}


void Dictionary::rank_suggestions(const string& word, vector<string> &suggestions) const{

	multimap< unsigned int, string > localmap;

	for(vector<string>::iterator it = suggestions.begin(); it != suggestions.end(); ++it) {
		string w = *it;
		unsigned int aaa = get_ed_dist(word, w);
		// cout << w << " " << aaa << endl;
		localmap.insert(std::make_pair(aaa , w ));
	}
	vector<string> temp_sugg;

	for(unsigned int k = 0; k <= wordlen; k++){
		auto search = localmap.find(k);

		while(search != localmap.end()){

			temp_sugg.insert(temp_sugg.end(), search->second);
			localmap.erase(search);
			search = localmap.find(k);
			// cout << "skriat\n"<< endl;
		}
	}

	temp_sugg.swap(suggestions);
}




void Dictionary::trim_suggestions( vector<string> &suggestions) const{
	const int max_suggs = 5;
	while (suggestions.size() > max_suggs){
		suggestions.pop_back();
	}
}





vector<string> Dictionary::get_suggestions(const string& word) const {
	vector<string> suggestions;
	Dictionary::gather_suggestions( word, suggestions);
	Dictionary::rank_suggestions( word, suggestions);
	// cout << suggestions.size()<< endl;
	trim_suggestions(suggestions);

	return suggestions;
}
