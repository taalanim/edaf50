#ifndef WORD_H
#define WORD_H

#include <string>
#include <vector>

class Word {
public:
	/* Creates a word w with the sorted trigrams t */
	Word(const std::string& w, const std::vector<std::string>& t): word(w), tripps(t){}

	void print() const;
	/* Returns the word */
	std::string get_word() const {return word;};

	/* Returns how many of the trigrams in t that are present
	 in this word's trigram vector */
	unsigned int get_matches(const std::vector<std::string>& t) const;

private:
	std::string word;
	std::vector<std::string> tripps;

};

#endif
