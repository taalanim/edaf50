#include <iostream>
#include "list.h"
List::List() {
	first = nullptr;
}

List::~List() {
	while (first != nullptr){
		remove_first();
	}
}

bool List::exists(int d) const {
	if (first == nullptr){
		return false;
	}
	bool found = false;
	Node *current = first;
	while(!found && current->next != nullptr ){
		if(current->value == d){
			found = true;
		}
		current = current->next;
	}
	if(current->value == d){
		found = true;
	}

	return found;
}

int List::size() const {
	if (first == nullptr){
		return 0;
	}

	int k = 1;

	Node *current = first;
	while(current->next != nullptr){
		current = current->next;
		k++;
	}

	return k;
}

bool List::empty() const {
	return first == nullptr;
}

void List::insertFirst(int d) {
	Node *temp;
	if(first != nullptr){
		temp = first;
		first =  new Node(d, temp);
	} else{
		first = new Node(d,nullptr);
	}
}


void List::remove_first(){
	Node *a = first;

	if(first->next != nullptr){
		first = first->next;
	} else {
		first = nullptr;
	}
	delete a;
}

void List::remove_child(Node* node){
	Node *a = node->next;
	node->next = a->next;
	delete a;
}

void List::remove(int d, DeleteFlag df) {
	if (first == nullptr){
		return;
	}
	bool found = false;
	switch(df) {
		case DeleteFlag::EQUAL :
		if (first->value == d){
			remove_first();
			found = true;
		}
		break;

		case DeleteFlag::GREATER :
		if (first->value > d){
			remove_first();
			found = true;
		}
		break;

		case DeleteFlag::LESS :
		if (first->value < d){
			remove_first();
			found = true;
		}
		break;
	}
	if (!found) {
		Node *current = first->next;
		Node *old = first;
		while(current->next != nullptr && !found){
			old = current;
			switch(df) {
				case DeleteFlag::EQUAL :
				if (current->next->value == d){
					remove_child(old);
					found = true;
				}
				break;
				case DeleteFlag::GREATER :
				if (current->next->value > d){
					remove_child(old);
					found = true;
				}
				break;
				case DeleteFlag::LESS :
				if (current->next->value < d){
					remove_child(old);
					found = true;
				}
				break;
			}
			current = current->next;
		}
	}


}



void List::print() const {
	if(empty()){
		return;
	}


	Node *current = first;
	while(current->next != nullptr){
		printf("%d, ", current->value);
		current = current->next;
	}
	printf("%d\n", current->value);

}



// void List::remove_EQ(int d){
// 	Node *current;
// 	Node *old;
//
// 	while (first->value == d){
// 		remove_first();
// 		if(first == nullptr){
// 			return;
// 		}
// 	}
// 	current = first->next;
// 	old = first;
//
// 	while(current->next != nullptr){
// 		old = current;
// 		if (current->next->value == d){
// 			remove_child(old);
// 		} else{
// 			current = current->next;
// 		}
// 	}
// }
//
//
// void List::remove_GR(int d){
// 	Node *current;
// 	Node *old;
//
// 	while (first->value > d){
// 		remove_first();
// 		if(first == nullptr){
// 			return;
// 		}
// 	}
// 	current = first->next;
// 	old = first;
//
// 	while(current->next != nullptr){
// 		old = current;
// 		if (current->next->value > d){
// 			remove_child(old);
// 		} else{
// 			current = current->next;
// 		}
// 	}
// }
//
//
// void List::remove_LE(int d){
// 	Node *current;
// 	Node *old;
//
// 	while (first->value < d){
// 		remove_first();
// 		if(first == nullptr){
// 			return;
// 		}
// 	}
// 	current = first->next;
// 	old = first;
//
// 	while(current->next != nullptr){
// 		old = current;
// 		if (current->next->value < d){
// 			remove_child(old);
// 		} else{
// 			current = current->next;
// 		}
// 	}
// }
//
//
//
// void List::remove_old(int d, DeleteFlag df) {
// 	if (first == nullptr){
// 		return;
// 	}
// 	switch(df) {
// 		case DeleteFlag::EQUAL :
// 		remove_EQ(d);
// 		break;
//
// 		case DeleteFlag::GREATER :
// 		remove_GR(d);
// 		break;
//
// 		case DeleteFlag::LESS :
// 		remove_LE(d);
// 		break;
// 	}
// }
