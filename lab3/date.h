#ifndef DATE_H
#define DATE_H
#include <iostream>
#include <string>
#include <cstdlib>

class Date {
public:
	Date();                    // today's date
	Date(std::string r);                    // today's date
	Date(int y, int m, int d):year(y), month(m), day(d){} // yyyy-mm-dd
	int getYear() const;       // get the year
	int getMonth() const;      // get the month
	int getDay() const;        // get the day
	void next();               // advance to next day
	
	friend std::istream& operator >> (std::istream& is, Date& obj) {
		int a;
		int b;
		int c;
		
		is >> a;
		is >> b;
		is >> c;
		
		a = abs(a);
		b = abs(b);
		c = abs(c);
		
		obj.year = a; 
		obj.month = b; 
		obj.day = c;
		
		return is;
	}
	
	friend std::ostream& operator<< (std::ostream& stream, const Date& obj) {
		stream << obj.year << "-" << obj.month << "-" << obj.day << std::endl;
		return stream;
	}
	
	
	
private:
	int year;  // the year (four digits)
	int month; // the month (1-12)
	int day;   // the day (1-..)
	static int daysPerMonth[12]; // number of days in each month
};

#endif


