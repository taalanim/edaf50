#ifndef DATE_H
#define DATE_H

#include <fstream>

class TagRemover {
public:

	// read from cin
	TagRemover(std::istream& stream);

	// print on cout
	void print(std::ostream& stream) const;

private:
	std::istream& in_stream;

};

#endif
